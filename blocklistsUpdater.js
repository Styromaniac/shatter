// Function to update blocklists
function updateBlocklists() {
    const pattern = /^(https:\/\/|http:\/\/localhost|http:\/\/127\.0\.0\.1|http:\/\/\[.*\]|.*\.ipfs\.localhost:8080|.*\.ipns\.localhost:8080|.*\.loki|.*\.onion|.*\.i2p)/;

    // Retrieve linked files for both categories
    ["url", "query"].forEach(category => {
        browser.storage.local.get(category + "LinkedChecksumFiles").then((result) => {
            let linkedFiles = result[category + "LinkedChecksumFiles"] || [];
            linkedFiles.forEach(url => {
                // Reuse the logic you had in the linkChecksumFile function
                if (pattern.test(url)) { // Using regular expression pattern
                    fetch(url)
                        .then(response => response.text())
                        .then(data => {
                            let checksumsFromFile = data.split('\n').filter(line => line.trim() !== '');
                            // Directly set the new checksums, replacing the old ones
                            browser.storage.local.set({ [category + "Checksums"]: checksumsFromFile });
                        })
                        .catch(error => console.error(error));
                } else {
                    console.warn("Invalid URL. The URL must start with ...");
                }
            });
        });
    });
}

// Schedule the updateBlocklists function to run every 24 hours
const updateInterval = 24 * 60 * 60 * 1000; // 24 hours
setInterval(updateBlocklists, updateInterval);

// Optionally, run updateBlocklists immediately when the extension starts
updateBlocklists();