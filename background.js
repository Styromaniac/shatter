// Helper function for getting a checksum of a URL
function hashTextWithSHA3_512(text) {
    const shaObj = new jsSHA("SHA3-512", "TEXT", { encoding: "UTF8" });
    shaObj.update(text);
    return shaObj.getHash("HEX");
}

const searchEngines = {
    'google': 'q',
    'bing': 'q',
    'duckduckgo': 'q',
    'aol': 'q',
    'ask': 'q',
    'dogpile': 'q',
    'ecosia': 'q',
    'mojeek': 'q',
    'webcrawler': 'q',
    'info': 'q',
    'zapmeta': 'q',
    'hotbot': 'q',
    'teoma': 'q',
    'contenko': 'q',
    'yandex': 'text',
    'baidu': 'wd',
    'sogou': 'query',
    'swisscows': 'query',
    'metager': 'eingabe'
};

function getSearchQueryFromURL(url) {
  const urlObj = new URL(url);
  const hostnameParts = urlObj.hostname.split('.');
  const baseDomain = hostnameParts.length > 1 ? hostnameParts.slice(-2).join('.') : urlObj.hostname;
  const queryParam = searchEngines[baseDomain.split('.').slice(-2)[0]] || null;

  if (queryParam) {
    const query = urlObj.searchParams.get(queryParam);
    return query ? query.replace(/\\+/g, ' ') : null;
  }
  return null;
}

// In-memory cache for checksums
let queryChecksumsCache = new Set();
let urlChecksumsCache = new Set();

// Function to load checksums from storage to cache
function loadChecksums() {
    browser.storage.local.get(["queryChecksums", "urlChecksums"]).then((result) => {
        queryChecksumsCache = new Set(result.queryChecksums || []);
        urlChecksumsCache = new Set(result.urlChecksums || []);
    });
}

// Listen for changes in the storage and update the cache accordingly
browser.storage.onChanged.addListener((changes, areaName) => {
    if (areaName === 'local') {
        if (changes.queryChecksums) {
            queryChecksumsCache = new Set(changes.queryChecksums.newValue || []);
        }
        if (changes.urlChecksums) {
            urlChecksumsCache = new Set(changes.urlChecksums.newValue || []);
        }
    }
});

// Load the checksums into the cache when the extension starts
loadChecksums();

browser.webRequest.onBeforeRequest.addListener(
    (details) => {
        let searchQuery = getSearchQueryFromURL(details.url);
        if (searchQuery) {
            let checksum = hashTextWithSHA3_512(searchQuery);
            if (queryChecksumsCache.has(checksum)) { // Use 'has' instead of 'includes'
                return { redirectUrl: browser.extension.getURL("warning.html") };
            }
        }
        return {cancel: false};
    },
    {urls: ["<all_urls>"]},
    ["blocking"]
);

function getChecksum(url) {
    const urlObj = new URL(url);
    const parts = urlObj.hostname.split('.');
    const topLevelDomain = parts.length > 1 ? parts.slice(-2).join('.') : urlObj.hostname;
    return hashTextWithSHA3_512(topLevelDomain);
}

browser.webRequest.onBeforeRequest.addListener(
    (details) => {
        let checksum = getChecksum(details.url);
        if (urlChecksumsCache.has(checksum)) { // Use 'has' instead of 'includes'
            return { redirectUrl: browser.extension.getURL("warning.html") };
        }
        return {cancel: false};
    },
    {urls: ["<all_urls>"]},
    ["blocking"]
);